package sk.golias.pavol.annotations;

import java.lang.reflect.Method;

// based on: https://www.javatpoint.com/custom-annotation

public class Example1 {
    public static void main(String[] args) throws NoSuchMethodException {
        TestClass testClass = new TestClass();

        Method m1 = testClass.getClass().getMethod("printSomething");
        CustomAnnotation annotation = m1.getAnnotation(CustomAnnotation.class);
        System.out.println("Annotation value for method 1: " + annotation.value());

        Method m2 = testClass.getClass().getMethod("printSomething2");
        annotation = m2.getAnnotation(CustomAnnotation.class);
        System.out.println("Annotation value for method 1: " + annotation.value());
    }
}
