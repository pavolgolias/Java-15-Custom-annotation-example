package sk.golias.pavol.annotations;

public class TestClass {

    @CustomAnnotation(value = 5)
    public void printSomething() {
        System.out.println("Test print");
    }

    @CustomAnnotation()
    public void printSomething2() {
        System.out.println("Test print");
    }
}
